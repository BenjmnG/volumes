#!/usr/bin/env node
"use strict";

const passThrough							= require('./generator/lib/passThrough.js'),
			fs 							  			= require("fs"),
      path             				= require('path'),
      config           				= require(path.join(process.cwd(), 'config.js')),
			bs							  			= require("browser-sync").create(),
			{execSync} 							= require('child_process'),
			sass 										= require('sass')

const { generateStaticSite }	= require("./generator/index.js")


const 	postsdir =  config.dev.postsdir,
			 assetsdir =  config.dev.assetsdir,
					outdir =  config.dev.outdir


const build = () => {
	
	// clean current Export directory and regen files
	if (!fs.existsSync(config.dev.outdir)){
		fs.rmSync(outdir, { recursive: true, force: true });
		fs.mkdirSync(outdir);
	} 
	generateStaticSite()
	passThrough(assetsdir, `${outdir}/assets/`, /scss/)
}


const serve = () =>{

	function writeCss(){
		let result = sass.compile("./design/assets/scss/main.scss")
		if(result.css == undefined){ result.css = '' }
		fs.writeFileSync('./design/assets/css/main.css', result.css, 'utf8');
	}
	
	// Compile SCSS once
	writeCss()

	// Watch SCSS next
	bs.watch(`./design/assets/scss/`).on("change", writeCss)
	
	// Watch design/assets
	// @param {string} ext folder and extension to watch
	const assets = ['js', 'css', 'svg']
	assets.forEach( ext => {
		bs.watch(`./design/assets/${ext}/*.${ext}`, function (event, file) {
	    if (event === "change") {
	    			passThrough(`${assetsdir}/${ext}`, `${outdir}/assets/${ext}`)
	        	bs.reload();
	    }
		});
	})

	bs.watch(`./design/assets/modules/`).on("change", bs.exit)

	// Watch layout change
	bs.watch("design/layouts", function (event, file) {
	  if (event === "change") {
	  		generateStaticSite()
	      bs.reload();
	  }
	});

	// Watch libraries change
	bs.watch("design/assets/lib", function (event, file) {
	  if (event === "change") {
	  		generateStaticSite()
	      bs.reload();
	  }
	});

	// Watch contenu change
	bs.watch("contenu/**", function (event, file) {
	  if (event === "change") {
	  		generateStaticSite()
				passThrough(assetsdir, `${outdir}/assets/`, /scss/)
	      bs.reload();
	  }
	});

	bs.init({
	    server: outdir,
			open: false,
			port: 8080
	});
}


/** This dispatch demande between two scenarios */
console.log("[Volumes] Let's " + process.argv[2])

if(process.argv[2] == "build"){
	build()
} else if(process.argv[2] == "serve"){
	build()
	serve()
}