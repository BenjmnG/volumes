const fs = require("fs")
const path = require("path")

/**
 * Look ma, it's cp -R.
 * @param {string} src      The path to the thing to copy.
 * @param {string} dest     The path to the new copy.
 * @param {string} exclude  Prevent this extension to be copied.
 */

var passThrough = function(src, dest, exclude) {
  var exists = fs.existsSync(src);
  var stats = exists && fs.statSync(src);
  var isDirectory = exists && stats.isDirectory();
  if (isDirectory) {
    if (!fs.existsSync(dest)){
      fs.mkdirSync(dest);
    }
    fs.readdirSync(src).forEach(function(childItemName) {
      let ext = childItemName.split('.').pop();
      //console.log(childItemName,exclude, ext)
      if(!exclude || exclude && !exclude.test(ext) ){
       passThrough(path.join(src, childItemName),
                         path.join(dest, childItemName));
      }
    });
  } else {
    fs.copyFileSync(src, dest);
  }

  // Delete output dir if empty
  fs.rmdir(dest, (err) => {
    if (err) {
        //console.log(err);
    } else {
        console.log('\x1B[35m' + dest + " directory is empty. Deleted successfully");
    }
  })
};

module.exports = passThrough