const	mdAttrs = require('markdown-it-attrs'),
      mdFn    = require('markdown-it-footnote'),
      md      = require('markdown-it')({html: true,linkify: false,typographer: true}),
      markdownLibrary = md.use(mdAttrs).use(mdFn)

const mdParser = content => md.render(content);

module.exports = { mdParser }