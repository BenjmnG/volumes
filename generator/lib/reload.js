const {passThrough}			= require('./lib/passThrough.js'),
			fs 							  = require("fs"),
			sass 							= require("sass"),
			bs							  = require("browser-sync").create();


const env = process.argv[2]

// erase current Export directory
fs.rmSync('export', { recursive: true, force: true });

// Watch design/assets
['js', 'css', 'svg'].forEach( ext => {
	bs.watch(ext + "/*." + ext, function (event, file) {
    if (event === "change") {
    		passThrough('design/assets/' + ext, 'export/' + ext)
        bs.reload("*." + ext);
    }
	});
})

bs.watch("contenu/*.**", function (event, file) {
  if (event === "change") {
      bs.reload();
  }
});

bs.init({
    server: "./export",
		open: false
});
