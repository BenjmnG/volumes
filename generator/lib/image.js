const Image = require('@11ty/eleventy-img')

async function imageShortcode(src, alt = "", sizes="1280px") {
  
  let metadata = await Image('./contenu/media/'+src, {
    widths: [1280],
    formats: ["webp", "jpg"],
    outputDir: "./public/media/couverture",
    urlPath: "../media/couverture",
    filenameFormat: function (id, src, width, format, options) {
      const extension = path.extname(src);
      const name = path.basename(src, extension);
      return `${name}-${width}.${format}`;
    }    
  });

  let imageAttributes = {
    alt,
    sizes,
    loading: "lazy",
    decoding: "async"
  };
  return Image.generateHTML(metadata, imageAttributes, {
    whitespaceMode: "inline"
  });
}

module.exports = { imageShortcode }