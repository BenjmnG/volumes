const fs                = require("fs"),
      fm                = require("front-matter"),
      {mdParser}        = require('./lib/markdown.js'),
      path              = require('path'),
      config            = require(path.join(process.cwd(), 'config.js')),
      passThrough       = require('./lib/passThrough.js'),
      nunjucks          = require('nunjucks'),
      sass              = require('sass');

const postsdir = config.dev.postsdir,
      outdir = config.dev.outdir


/**
Find and parse all ressource in directory
*/
function utils_buildAssets(folder){
  
  const assets = fs.readdirSync(folder)

  let txts =  assets.filter( file => file.endsWith('txt') || file.endsWith('md') ),
      css  =  assets.filter( file => file.endsWith('.css') ),
      scss =  assets.filter( file => file.endsWith('.scss') ),
      js   =  assets.filter( file => file.endsWith('.js') ),
      data =  assets.filter( file => file.endsWith('.json') ),
      imgs =  assets.filter( file => file.endsWith('jpg')  || file.endsWith('png') );
  
  // if data exist 
  if(
    fs.existsSync(`${folder}/${data}`) 
    && !fs.lstatSync(`${folder}/${data}`).isDirectory()
  ){
    data = fs.readFileSync(`${folder}/${data}`, "utf8");
    data = JSON.parse(data)
  }

  return {
    type: 'common',
    txt: txts,
    css: css,
    data: data,
    img: imgs,
    js: js
  }
}

/**
*  Function return an array of parsed content
*  @param {string} content.body         Body content parsed in Html content  
*  @param {string} content.attributes   Frontmatter content
*/
function utils_parseContent(file, name){
  const data = fs.readFileSync(`${postsdir}/${name}/${file}`, "utf8");
  const content = fm(data);
  content.filename = file;
  content.body = mdParser(content.body);
  return content;
}

class Common{

  constructor(){
    this.name = "_common";
    this.path = "_common";
  }

  /**
  *  This function return an object of common ressource
  */
  buildCommonAssets(folder){

    let assets = utils_buildAssets(folder)

    passThrough(`${postsdir}/_common`, `${outdir}/_common/`, /txt|json|md/ )

    assets.type = "common"
    
    return assets
  }

  /**
  *  This function call global parseContent 
  */
  parseContent(file){
    return utils_parseContent(file, '_common')
  }
}


class Page{

  constructor({ name, path }){
    this.name = name;
    this.path = path
  }

  /**
  *  This function return an object of Page ressource
  */
  buildPageAssets(name, index){

    const folder = `${postsdir}/${this.path}/`

    let assets = utils_buildAssets(folder)

    if(!assets.data.title){
      let title = name.match(/(?<=-).*/gm)
      assets.data = Object.create({title: title})
    }

    passThrough(folder, `${outdir}/${name}/`, /txt|json|md/ )


    assets.type   = "issue"
    assets.issue  = index + 1
    assets.path   = `${name}.html`

    return assets
  }

  /**
  *  This function call global parseContent
  */
  parseContent(file, name){
    return utils_parseContent(file, name)
  }

  /**
    *  This function render page with nunjuck 
  */
  render(template, content){
    const env = nunjucks.configure('design/layouts/', { autoescape: true });
    
    // Add Filter
    env.addFilter('isString', something => typeof something == 'string')
    env.addFilter('isArray', something => Array.isArray(something))
    env.addFilter('shuffle', values => values.sort((a, b) => 0.5 - Math.random()) )

    //console.log(content)
    return env.render(template, content);
  }

  /**
   * Finally write a proper HTML page
   * @param {string}  DOM    HTML DOM to write on file
   * @param {string}  path   Path to write file
   */
  writeFile(DOM, path){
    fs.writeFile(
      `${path}.html`,
      DOM,
      e => {
        if (e) throw e;
        console.log(`\x1B[32m${path}.html was created successfully\x1B[0m`);
      }
    );
  }
}


module.exports = {Page, Common}