const fs                = require("fs"),
      path              = require('path'),
      config            = require(path.join(process.cwd(), 'config.js')),
      {Page, Common}    = require("./utils.js"),
      passThrough       = require('./lib/passThrough.js')


const postsdir = config.dev.postsdir,
      outdir = config.dev.outdir

let issues = [],
    common = []

/*
 * @param {array} contents  Array of all parsed pages
*/
class site{
  
  static findDirectories(){
    let dir = fs.readdirSync(postsdir)
    let dir_without_underscore = []
    dir.forEach(s => {
      if (!s.startsWith('_')) {
        dir_without_underscore.push(s);
      }
    })
    return dir_without_underscore
  }

  static common(){
    let folder = `${postsdir}/_common/`
    if(fs. existsSync(folder)){
      const c = new Common()
      common = c.buildCommonAssets(folder)
      
      console.log(`Find ${common.txt.length} txt file in common folder`)

      common.txt = common.txt
        .map(file => c.parseContent(file, "_common"))

      return common
    }
  }

  /**
   * This function compose Page HTML and add assets
   * @param {object}  page_Issue  Page to generate and write. Construct on Page (./utils.js) 
   */ 
  static page(directory, index){

    const page_Issue = new Page({
      name: directory,
      path: `${directory}`
    })

    let pageAssets = page_Issue.buildPageAssets(directory, index)
    console.log(`Find ${pageAssets.txt.length} txt file in folder ${directory}`)
    
    
    pageAssets.txt = pageAssets.txt
      .map(file => page_Issue.parseContent(file, directory))

    issues.push(pageAssets)

    // Add common assets to page before render
    if(common.type){
      pageAssets.common = common
      //console.log(pageAssets.common.txt[0].attributes)
    }
    

    const DOM = page_Issue.render('issue.html', pageAssets)

    page_Issue.writeFile(DOM, `${outdir}/${directory}`)


  }

  /**
   * This function create homepage and list issues (aka subpages)
   * @param {array}   directories   array of all directories found
   */
  static home(){
  
    const page_Index = new Page({
      name: 'index',
      path: ''
    })
  
    const DOM = page_Index.render('index.html', {issues: issues, type: 'index'})

    page_Index.writeFile(DOM, `${outdir}/index`)
  }

}

function generateStaticSite(){
  // reset Issues
  issues = []

  // find directory
  const directories = site.findDirectories()

  // create common ressources
  site.common()
    
  // REad comon folder and check for common txt

  // create pages
  directories.map(
    (directory, index) => site.page(directory, index)
  )

  // create home
  site.home(issues)
}




module.exports = { generateStaticSite }