#!/usr/bin/env node
"use strict";

/** This file initiate assets and layout once for all */

const fs          = require('fs'),
      path        = require('path'),
      passThrough = require('./generator/lib/passThrough.js')


function createFile(folderPath, fileName, content) {
  const filePath = path.join(folderPath, fileName);
  fs.writeFileSync(filePath, content, 'utf8');
  console.log(`Created blank file: ${filePath}`);
}


/** 
 * Create Config file in root project
 */

const config = {
  dev: {
    postsdir: "./contenu",
    assetsdir: "./design/assets",
    outdir: "./export"
  },
  filter:{
    
  }
};

try {
  fs.writeFileSync('./config.js', `module.exports = ${JSON.stringify(config, null, 2)}`);
  console.log('Configuration file created successfully');
} catch (error) {
  console.error('Error creating configuration file:', error);
}


/**
 * Create essential folder
 */

const foldersToCreate = [
  'contenu',
  'contenu/example',
  'design',
  'design/assets',
  'design/assets/js',
  'design/assets/lib',
  'design/assets/css',
  'design/assets/scss'
];

foldersToCreate.forEach(folderName => {
  const folderPath = path.join(process.cwd(), folderName);

  try {
    fs.mkdirSync(folderPath);
    if (folderName === 'design/assets/scss') {
      // This create Scss file on a particular scenario
      createFile(folderPath, 'main.scss', 'p{font-family: monospace;}');
    } else if (folderName === 'contenu/example') {
      createFile(folderPath, '_data.json', '{"title": "Exemple","author": ["Jane Doe"]}');
      createFile(folderPath, 'my_first_text.txt', "Bonjour");
    } else {
      console.log(`Created folder: ${folderName}`);
    }
  } catch (error) {
    if (error.code === 'EXIST') {
      console.log(`Folder already exists: ${folderName}`);
    } else {
      console.error(`Error creating folder: ${folderName}`, error);
    }
  }
});

/** 
 * Duplicate basic nunjuck layout
 */
passThrough(`${__dirname}/layouts`, `${process.cwd()}/design/layouts`)

/** 
 * Duplicate basic UI assets
 */
// Home
passThrough(`${__dirname}/ui/home/js`, `${process.cwd()}/design/assets/js`)
passThrough(`${__dirname}/ui/home/css`, `${process.cwd()}/design/assets/css`)
// Issue
passThrough(`${__dirname}/ui/issue/js`, `${process.cwd()}/design/assets/js`)
passThrough(`${__dirname}/ui/issue/css`, `${process.cwd()}/design/assets/css`)
passThrough(`${__dirname}/ui/issue/lib`, `${process.cwd()}/design/assets/lib`)