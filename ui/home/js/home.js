let isPrint = document.querySelector('#isPrint')

// If is Print, add paramter Print to url link
// This parameter allow display content as paged

function updatePrintParameter() {
  const isPrintChecked = document.querySelector('#isPrint').checked;

  // Get all links in the DOM.
  const links = document.querySelectorAll('a.volume');

  // Iterate over all links.
  for (const link of links) {
    // Get the link's href.
    const href = link.getAttribute('href');

    // If the checkbox is checked, add the `?print` parameter to the href.
    if (isPrintChecked) {
      // If the href already contains the `?print` parameter, skip it.
      if (href.includes('?print')) {
        continue;
      }

      // Add the `?print` parameter to the href.
      link.setAttribute('href', href + '?print');
    } else {
      // If the href contains the `?print` parameter, remove it.
      if (href.includes('?print')) {
        // Split the href at the `?print` parameter.
        const [hrefWithoutPrint, printParameter] = href.split('?print');

        // Set the link's href to the href without the `?print` parameter.
        link.setAttribute('href', hrefWithoutPrint);
      }
    }
  }
}

updatePrintParameter()
isPrint.onchange = updatePrintParameter;
