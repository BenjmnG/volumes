function loadJS(url, callback) {
    let script_el = document.createElement('script')
    script_el.src = url
    script_el.onload = callback
    document.body.appendChild(script_el)
}

let url = window.location.href

if (url.includes('?print') && !navigator.userAgent.includes("HeadlessChrome")){

  window.PagedConfig = { auto: false }

  document.body.classList = 'print';
  document.querySelector('main').classList = 'print';

  // declare futur callback in order
  let loadReload = () => loadJS('assets/lib/reload_in_place.js', loadImposition)
  let loadImposition = () => loadJS('assets/lib/imposition.js', openPreview)
  let openPreview = () => window.PagedPolyfill.preview()

  // Start with
  loadJS('assets/lib/paged.js', loadReload);

}